<?php

use PHPUnit\Framework\TestCase;

class TacheTest extends TestCase
{
    public function testConstructor()
    {
        global $rep;
        $idTache = 1;
        $nom = 'Test Task';
        $terminee = false;
        $idListeTaches = 2;

        $tache = new Tache($idTache, $nom, $terminee, $idListeTaches);

        $this->assertEquals($idTache, $tache->getIdTache());
        $this->assertEquals($nom, $tache->getNom());
        $this->assertEquals($terminee, $tache->getTerminee());
        $this->assertEquals($idListeTaches, $tache->getIdListeTaches());
    }

    public function testSettersAndGetters()
    {
        $tache = new Tache(1, 'Test Task', false, 2);

        // Test setters and getters
        $tache->setIdTache(3);
        $this->assertEquals(3, $tache->getIdTache());

        $tache->setNom('New Test Task');
        $this->assertEquals('New Test Task', $tache->getNom());

        $tache->setTerminee(true);
        $this->assertEquals(true, $tache->getTerminee());

        $tache->setIdListeTaches(4);
        $this->assertEquals(4, $tache->getIdListeTaches());
    }
}