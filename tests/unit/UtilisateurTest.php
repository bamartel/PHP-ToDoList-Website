<?php

use PHPUnit\Framework\TestCase;

require_once(__DIR__ . "/../../metier/Utilisateur.php");

class UtilisateurTest extends TestCase
{
    public function testConstructor()
    {
        $pseudo = 'testuser';

        $utilisateur = new Utilisateur($pseudo);

        $this->assertEquals($pseudo, $utilisateur->getPseudo());
    }

    public function testSettersAndGetters()
    {
        $utilisateur = new Utilisateur('testuser');

        // Test setters and getters
        $utilisateur->setPseudo('newuser');
        $this->assertEquals('newuser', $utilisateur->getPseudo());
    }
}