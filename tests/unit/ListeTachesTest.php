<?php

use PHPUnit\Framework\TestCase;

require(__DIR__ . "/../../config/Config.php");

class ListeTachesTest extends \PHPUnit\Framework\TestCase
{
    
    public function testConstructor()
    {
        $idListeTaches = 1;
        $nom = 'Test List';
        $confidentialite = true;
        $description = 'This is a test description';

        $listTaches = new ListeTaches($idListeTaches, $nom, $confidentialite, $description);

        $this->assertEquals($idListeTaches, $listTaches->getIdListeTaches());
        $this->assertEquals($nom, $listTaches->getNom());
        $this->assertEquals($confidentialite, $listTaches->getConfidentialite());
        $this->assertEquals($description, $listTaches->getDescription());
    }

    public function testSettersAndGetters()
    {
        $listTaches = new ListeTaches(1, 'Test List', true, 'This is a test description');

        // Test setters and getters
        $listTaches->setIdListeTaches(2);
        $this->assertEquals(2, $listTaches->getIdListeTaches());

        $listTaches->setNom('New Test List');
        $this->assertEquals('New Test List', $listTaches->getNom());

        $listTaches->setConfidentialite(false);
        $this->assertEquals(false, $listTaches->getConfidentialite());

        $listTaches->setDescription('New description');
        $this->assertEquals('New description', $listTaches->getDescription());
    }
    
}




