<?php

use PHPUnit\Framework\TestCase;

require(__DIR__ . "/../../config/Config.php");

class ConnexionTest extends \PHPUnit\Framework\TestCase 
{
    public function testConnexion()
    {
        global $dsn, $login, $mdp;
        $dsn = $dsn != "" ? $dsn : $GLOBALS['dsn'];
        $login = $login != "" ? $login : $GLOBALS['login'];
        $mdp = $mdp != "" ? $mdp : $GLOBALS['mdp'];
        $conn = new Connexion($dsn, $login, $mdp);
        $conn-> __construct($dsn, $login, $mdp);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->assertInstanceOf(PDO::class, $conn);
    }
}