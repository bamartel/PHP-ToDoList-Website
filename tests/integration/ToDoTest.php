<?php

use PHPUnit\Framework\TestCase;

class ToDoTest extends \PHPUnit\Framework\TestCase 
{


//test de la fonction d'insert d'une tache
    public function testSavingTache()
    {
        
        global $dsn, $login, $mdp;
        $dsn = $dsn != "" ? $dsn : $GLOBALS['dsn'];
        $login = $login != "" ? $login : $GLOBALS['login'];
        $mdp = $mdp != "" ? $mdp : $GLOBALS['mdp'];

        $connexion = new Connexion($dsn, $login, $mdp);
        $tacheGateway = new TacheGateway($connexion);

        $nomTache = "Nouvelle";
        $terminee = false;
        $idListeTaches = 1; 

        try {
            $tacheGateway->insertTache($nomTache, $terminee, $idListeTaches);
            $result = $connexion->query("SELECT * FROM Tache WHERE nom = '$nomTache' AND terminee = '$terminee' AND idListeTaches = '$idListeTaches'");
            $tache = $result->fetch();

            $this->assertNotEmpty($tache, 'La tâche n\'a pas été insérée dans la base de données.');

            $this->assertEquals($nomTache, $tache['nom']);
            $this->assertEquals($terminee, $tache['terminee']);
            $this->assertEquals($idListeTaches, $tache['idListeTaches']);

            echo "Nouvelle tâche insérée avec succès.";
        } catch (PDOException $e) {
            echo "Erreur d'insertion de tâche : " . $e->getMessage();
        }
    }
    
//test de la fonction de suppression par id de la tache
    public function testDeleteTacheById()
    {
        global $dsn, $login, $mdp;
        $dsn = $dsn != "" ? $dsn : $GLOBALS['dsn'];
        $login = $login != "" ? $login : $GLOBALS['login'];
        $mdp = $mdp != "" ? $mdp : $GLOBALS['mdp'];

        $connexion = new Connexion($dsn, $login, $mdp);
        $tacheGateway = new TacheGateway($connexion);


        $nomTache = "Tache a supp";
        $terminee = false;
        $idListeTaches = 1; 

        try {
            $tacheGateway->insertTache($nomTache, $terminee, $idListeTaches);
            $result = $connexion->query("SELECT * FROM Tache WHERE nom = '$nomTache' AND terminee = '$terminee' AND idListeTaches = '$idListeTaches'");
            $tache = $result->fetch();

            $this->assertNotEmpty($tache, 'La tâche n\'a pas été insérée dans la base de données.');
            $tacheGateway->deleteTachesByIdListeTaches($idListeTaches);
            $resultAfterDelete = $connexion->query("SELECT * FROM Tache WHERE idListeTaches = '$idListeTaches'");
            $tachesAfterDelete = $resultAfterDelete->fetchAll();
    
            $this->assertEmpty($tachesAfterDelete, 'Les tâches associées à la liste n\'ont pas été supprimées de la base de données.');

            echo "Nouvelle tâche insérée et supprimée avec succès.";
        } catch (PDOException $e) {
            echo "Erreur d'insertion/suppression de tâche : " . $e->getMessage();
        }

    }

    //test de la fonction de get d'une tache via son id 
    public function testGetTacheByIdTache()
    {
        global $dsn, $login, $mdp;
        $dsn = $dsn != "" ? $dsn : $GLOBALS['dsn'];
        $login = $login != "" ? $login : $GLOBALS['login'];
        $mdp = $mdp != "" ? $mdp : $GLOBALS['mdp'];

        $connexion = new Connexion($dsn, $login, $mdp);
        $tacheGateway = new TacheGateway($connexion);

        $nomTache = "Tache a récupérer";
        $terminee = false;
        $idListeTaches = 1;

        $tacheGateway->insertTache($nomTache, $terminee, $idListeTaches);

        // verifie si la tache est insérée correctement
        $result = $connexion->query("SELECT * FROM Tache WHERE nom = '$nomTache' AND terminee = '$terminee' AND idListeTaches = '$idListeTaches'");
        $tache = $result->fetch();
        $this->assertNotEmpty($tache, 'La tâche n\'a pas été insérée dans la base de données.');

        // get de la tache par son Id
        $idTache = $tache['idTache'];
        $retrievedTache = $tacheGateway->getTacheByIdTache($idTache);
        // verifie si la tache recupérée est non nulle 
        $this->assertNotNull($retrievedTache, 'La tâche récupérée est null.');


    }
}